﻿using System;
using System.Linq;
using BinaryTask3.DAL.EF;
using BinaryTask3.WebAPI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BinaryTask5.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup: class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            //base.ConfigureWebHost(builder);
            builder.ConfigureServices(services =>
            {

                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                         typeof(DbContextOptions<ProjectsContext>));

                services.Remove(descriptor);

                services.AddDbContext<ProjectsContext>(options => options.UseInMemoryDatabase(
                    "Server=(localdb)\\mssqllocaldb;Database=ProjectsDatabaseTest;Trusted_Connection=True;"));
            });
        }
    }
}
