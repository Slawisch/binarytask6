﻿using AutoMapper;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public abstract class BaseService<T> where T : class
    {
        private protected readonly IMapper Mapper;
        private protected readonly IRepository<T> Repository;

        public BaseService(IRepository<T> repository, IMapper mapper)
        {
            Repository = repository;
            Mapper = mapper;
        }
    }
}
