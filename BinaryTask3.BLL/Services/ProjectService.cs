﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class ProjectService : BaseService<Project>, IProjectService
    {
        public ProjectService(IRepository<Project> repository, IMapper mapper) : base(repository, mapper) {}


        public async Task<IEnumerable<ProjectDTO>> GetProjectsAsync()
        {
            return Mapper.Map<IEnumerable<ProjectDTO>>(
                await Repository.ReadAsync());
        }

        public async Task<ProjectDTO> GetProjectAsync(int id)
        {
            try
            {
                return Mapper.Map<ProjectDTO>(
                    await Repository.ReadAsync(id));
            }
            catch
            {
                throw;
            }
        }

        public async Task UpdateProjectAsync(ProjectDTO project)
        {
            try
            {
                await Repository.UpdateAsync(Mapper.Map<Project>(project));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task CreateProjectAsync(ProjectDTO project)
        {
            try
            {
                var g = Mapper.Map<Project>(project);
                await Repository.CreateAsync(g);
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task DeleteProjectAsync(int id)
        {
            try
            {
                await Repository.DeleteAsync(id);
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }

        }

    }
}