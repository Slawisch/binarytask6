﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.CustomModels;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;
using BinaryTask3.DAL.Repositories;

namespace BinaryTask3.BLL.Services
{
    public class CustomService : ICustomService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<TaskEntity> _taskRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;

        private IEnumerable<ProjectDTO> _projects;
        private IEnumerable<TaskDTO> _tasks;
        private IEnumerable<TeamDTO> _teams;
        private IEnumerable<UserDTO> _users;

        private IEnumerable<ProjectInfo> _projectInfos;
        public CustomService(IMapper mapper,
            IRepository<Project> projectRepository,
            IRepository<TaskEntity> taskRepository,
            IRepository<Team> teamRepository,
            IRepository<User> userRepository)
        {
            _mapper = mapper;
            this._projectRepository = projectRepository;
            this._taskRepository = taskRepository;
            this._teamRepository = teamRepository;
            this._userRepository = userRepository;
        }

        private IEnumerable<ProjectInfo> GetProjectInfos()
        {
            var result = from p in _projects
                join t in _teams on p.TeamId equals t.Id
                join u in _users on p.AuthorId equals u.Id
                select new ProjectInfo()
                {
                    Id = p.Id,
                    TeamId = p.TeamId,
                    AuthorId = p.AuthorId,
                    Name = p.Name,
                    Description = p.Description,
                    Deadline = p.Deadline,
                    CreatedAt = p.CreatedAt,
                    Team = t,
                    Author = u,
                    Tasks = (from task in _tasks
                        join performer in _users on task.PerformerId equals performer.Id
                        where task.ProjectId == p.Id
                        select new TaskInfo()
                        {
                            Id = task.Id,
                            PerformerId = task.PerformerId,
                            ProjectId = task.ProjectId,
                            Performer = performer,
                            Description = task.Description,
                            CreatedAt = task.CreatedAt,
                            FinishedAt = task.FinishedAt,
                            Name = task.Name,
                            State = task.State
                        })
                };

            return result;
        }

        private async Task GetData()
        {
            _projects = _mapper.Map<IEnumerable<ProjectDTO>>(await _projectRepository.ReadAsync());
            _tasks = _mapper.Map<IEnumerable<TaskDTO>>(await _taskRepository.ReadAsync());
            _teams = _mapper.Map<IEnumerable<TeamDTO>>(await _teamRepository.ReadAsync());
            _users = _mapper.Map<IEnumerable<UserDTO>>(await _userRepository.ReadAsync());

            _projectInfos = GetProjectInfos();
        }

        private async Task GetDataAsync()
        {
            await Task.Run(GetData);
        }

        public async Task<Dictionary<ProjectDTO, int>> GetProjectTaskCountByUser(int id)
        {
            await GetDataAsync();
            try
            {
                var result = _projectInfos.Where(p => p.Author.Id == id)
                    .ToDictionary(
                        p => _projects.First(pr => pr.Id == p.Id),
                        p => p.Tasks.Count());

                return result;
            }
            catch
            {
                throw;
            }
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksByUserLess45(int id)
        {
            await GetDataAsync();
            try
            {
                var result = _projectInfos.SelectMany(p => p.Tasks)
                    .Where(t =>
                        t.Name != null &&
                        t.Performer.Id == id &&
                        t.Name.Length < 45);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async Task<IEnumerable<(int, string)>> GetTasksByUserDone(int id)
        {
            await GetDataAsync();
            try
            {
                var result = _projectInfos.SelectMany(p => p.Tasks)
                    .Where(t =>
                        t.Performer.Id == id &&
                        t.FinishedAt.HasValue &&
                        t.FinishedAt.Value.Year == DateTime.Now.Year).Select(t => (t.Id ?? 0, t.Name));
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Dictionary<int, IEnumerable<UserDTO>>> GetTeamsWithUsersOlder10()
        {
            await GetDataAsync();
            try
            {
                var result = _projectInfos.Select(p => p.Team).Distinct()
                    .GroupJoin(
                        _users,
                        team => team.Id,
                        user => user.TeamId,
                        (team, user) => (team.Id, team.Name, user))
                    .Where(item => item.user.All(us => us.BirthDay.Year < DateTime.Now.Year - 10)).ToDictionary(i => i.Id ?? 0, i => i.user);

                return result;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Dictionary<UserDTO, IEnumerable<TaskDTO>>> GetUsersWithTasks()
        {
            await GetDataAsync();
            try
            {
                var result = _users
                    .Join(_tasks,
                        u => u.Id,
                        t => t.PerformerId,
                        (u, t) => (u, t))
                    .GroupBy(ut => ut.u)
                    .SelectMany(g => g.OrderByDescending(grp => grp.t.Name?.Length))
                    .GroupBy(gr => gr.u)
                    .OrderBy(g => g.Key.FirstName);

                return result.ToDictionary(gr => gr.Key, gr => gr.Select(i => i.t));
            }
            catch
            {
                throw;
            }
        }

        public async Task<UserStruct> GetUserStruct(int id)
        {
            await GetDataAsync();
            try
            {
                var result = _projectInfos.Select(_ => new UserStruct()
                {
                    User = (_users.FirstOrDefault(u => u.Id == id)),
                    LastProject = (_projectInfos.Where(p => p.AuthorId == id)
                        .OrderBy(p => p.CreatedAt)?.Last()),
                    ProjectTaskCount = (_projectInfos.SelectMany(p => p.Tasks)
                        .Where(t => t.ProjectId ==
                                    (_projectInfos.Where(p => p.AuthorId == id)
                                        .OrderBy(p => p.CreatedAt)?.Last()).Id)).Count(),
                    UnfinishedTaskCount = (_projectInfos.SelectMany(p => p.Tasks)
                        .Where(t =>
                            t.Name != null &&
                            t.Performer.Id == id &&
                            t.FinishedAt == null)).Count(),
                    LongestTask = _projectInfos.SelectMany(p => p.Tasks)
                        .Where(t =>
                            t.Name != null &&
                            t.Performer.Id == id).OrderBy(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)?.Last()
                }).FirstOrDefault();

                return result;
            }
            catch
            {
                throw;
            }
        }

        public async Task<IEnumerable<ProjectStruct>> GetProjectStruct()
        {
            await GetDataAsync();
            try
            {
                var result = _projectInfos.Select(p => new ProjectStruct()
                {
                    Project = p,
                    LongestTask = (_projectInfos.SelectMany(pr => pr.Tasks)
                            .Where(t => t.ProjectId == p.Id))
                            .OrderBy(t => (t.Description ?? "").Length).LastOrDefault(),
                    ShortestTask = (_projectInfos.SelectMany(pr => pr.Tasks)
                            .Where(t => t.ProjectId == p.Id))
                            .OrderBy(t => (t.Name ?? "").Length).FirstOrDefault(),
                    UserCount = (_users
                        .Where(u =>
                            (u.TeamId ?? -1) == p.Team.Id &&
                            ((p.Tasks.Count() < 3) || (p.Description?.Length > 20)))
                        .Distinct().Count())
                });

                return result;
            }
            catch
            {
                throw;
            }
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUserId(int id)
        {
            await GetDataAsync();
            try
            {
                if(!_users.Select(u => u.Id).Contains(id))
                    throw new ArgumentException($"User with id: {id} not found");

                var result = _tasks.Where(t => t.PerformerId == id && t.FinishedAt == null);
                return result;
            }
            catch
            {
                throw;
            }
        }
    }
}
