﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class TeamService : BaseService<Team>, ITeamService
    {
        public TeamService(IRepository<Team> repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public async Task<IEnumerable<TeamDTO>> GetTeamsAsync()
        {
            return Mapper.Map<IEnumerable<TeamDTO>>(await Repository.ReadAsync());
        }

        public async Task<TeamDTO> GetTeamAsync(int id)
        {
            try
            {
                return Mapper.Map<TeamDTO>(await Repository.ReadAsync(id));
            }
            catch
            {
                throw;
            }
        }

        public async Task UpdateTeamAsync(TeamDTO team)
        {
            try
            {
                await Repository.UpdateAsync(Mapper.Map<Team>(team));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task CreateTeamAsync(TeamDTO team)
        {
            try
            {
                await Repository.CreateAsync(Mapper.Map<Team>(team));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task DeleteTeamAsync(int id)
        {
            try
            {
                await Repository.DeleteAsync(id);
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }
    }
}
