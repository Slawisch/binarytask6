﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class TaskService : BaseService<TaskEntity>, ITaskService
    {
        public TaskService(IRepository<TaskEntity> repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksAsync()
        {
            return Mapper.Map<IEnumerable<TaskDTO>>(await Repository.ReadAsync());
        }

        public async Task<TaskDTO> GetTaskAsync(int id)
        {
            try
            {
                return Mapper.Map<TaskDTO>(await Repository.ReadAsync(id));
            }
            catch
            {
                throw;
            }
        }

        public async Task UpdateTaskAsync(TaskDTO task)
        {
            try
            {
                await Repository.UpdateAsync(Mapper.Map<TaskEntity>(task));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task CreateTaskAsync(TaskDTO task)
        {
            try
            {
                await Repository.CreateAsync(Mapper.Map<TaskEntity>(task));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task DeleteTaskAsync(int id)
        {
            try
            {
                await Repository.DeleteAsync(id);
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task MarkTaskAsDoneAsync(int id)
        {
            try
            {
                var task = await Repository.ReadAsync(id);

                if(task.FinishedAtDate != null)
                    throw new InvalidOperationException("Task already done.");

                task.FinishedAtDate = DateTime.Now;
                await Repository.UpdateAsync(task);

                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task MarkAllTasksAsUndoneAsync()
        {
            try
            {
                await Task.Run(async () =>
                {
                    foreach (var item in await Repository.ReadAsync())
                    {
                        item.FinishedAtDate = null;
                    }
                });

                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }
    }
}
