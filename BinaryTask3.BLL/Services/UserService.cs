﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class UserService : BaseService<User>, IUserService
    {
        public UserService(IRepository<User> repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public async Task<IEnumerable<UserDTO>> GetUsersAsync()
        {
            return Mapper.Map<IEnumerable<UserDTO>>(await Repository.ReadAsync());
        }

        public async Task<UserDTO> GetUserAsync(int id)
        {
            try
            {
                return Mapper.Map<UserDTO>(await Repository.ReadAsync(id));
            }
            catch
            {
                throw;
            }
        }

        public async Task UpdateUserAsync(UserDTO user)
        {
            try
            {
                await Repository.UpdateAsync(Mapper.Map<User>(user));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task CreateUserAsync(UserDTO user)
        {
            try
            {
                await Repository.CreateAsync(Mapper.Map<User>(user));
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task DeleteUserAsync(int id)
        {
            try
            {
                await Repository.DeleteAsync(id);
                await Repository.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
        }
    }
}
