﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetProjectsAsync();
        Task<ProjectDTO> GetProjectAsync(int id);
        Task UpdateProjectAsync(ProjectDTO project);
        Task CreateProjectAsync(ProjectDTO project);
        Task DeleteProjectAsync(int id);
    }
}
