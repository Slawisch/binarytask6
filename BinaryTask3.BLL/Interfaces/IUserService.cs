﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetUsersAsync();
        Task<UserDTO> GetUserAsync(int id);
        Task UpdateUserAsync(UserDTO user);
        Task CreateUserAsync(UserDTO user);
        Task DeleteUserAsync(int id);
    }
}
