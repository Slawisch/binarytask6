﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;
using BinaryTask3.Common.CustomModels;

namespace BinaryTask3.BLL.Interfaces
{
    public interface ICustomService
    {
        public Task<Dictionary<ProjectDTO, int>> GetProjectTaskCountByUser(int id);
        public Task<IEnumerable<TaskDTO>> GetTasksByUserLess45(int id);
        public Task<IEnumerable<(int, string)>> GetTasksByUserDone(int id);
        public Task<Dictionary<int, IEnumerable<UserDTO>>> GetTeamsWithUsersOlder10();
        public Task<Dictionary<UserDTO, IEnumerable<TaskDTO>>> GetUsersWithTasks();
        public Task<UserStruct> GetUserStruct(int id);
        public Task<IEnumerable<ProjectStruct>> GetProjectStruct();
        public Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUserId(int id);
    }
}
