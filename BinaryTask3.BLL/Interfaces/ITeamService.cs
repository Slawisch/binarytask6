﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetTeamsAsync();
        Task<TeamDTO> GetTeamAsync(int id);
        Task UpdateTeamAsync(TeamDTO team);
        Task CreateTeamAsync(TeamDTO team);
        Task DeleteTeamAsync(int id);
    }
}
