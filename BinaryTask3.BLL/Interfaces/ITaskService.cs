﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetTasksAsync();
        Task<TaskDTO> GetTaskAsync(int id);
        Task UpdateTaskAsync(TaskDTO task);
        Task CreateTaskAsync(TaskDTO task);
        Task DeleteTaskAsync(int id);
        Task MarkTaskAsDoneAsync(int id);
        Task MarkAllTasksAsUndoneAsync();
    }
}
