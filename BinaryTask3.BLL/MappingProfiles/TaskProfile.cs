﻿using AutoMapper;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(x => x.CreatedAtDate))
                .ForMember(dest => dest.FinishedAt, src => src.MapFrom(x => x.FinishedAtDate));
            CreateMap<TaskDTO, TaskEntity>()
                .ForMember(dest => dest.CreatedAtDate, src => src.MapFrom(x => x.CreatedAt))
                .ForMember(dest => dest.FinishedAtDate, src => src.MapFrom(x => x.FinishedAt));
        }
    }
}
