﻿using BinaryTask3.Common.DTOs;

namespace BinaryTask3.Common.CustomModels
{
    public struct UserStruct
    {
        public UserDTO User { get; set; }
        public ProjectInfo LastProject { get; set; }
        public int ProjectTaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }
        public TaskInfo LongestTask { get; set; }

        public override string ToString()
        {
            return $"{User}\n{LastProject}\n{ProjectTaskCount}\n{UnfinishedTaskCount}\n{LongestTask}";
        }
    }
}