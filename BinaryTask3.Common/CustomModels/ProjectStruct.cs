﻿
namespace BinaryTask3.Common.CustomModels
{
    public struct ProjectStruct
    {
        public ProjectInfo Project { get; set; }
        public TaskInfo LongestTask { get; set; }
        public TaskInfo ShortestTask { get; set; }
        public int UserCount { get; set; }

        public override string ToString()
        {
            return $"{Project}\n{LongestTask}\n{ShortestTask}\n{UserCount}";
        }
    }

}