﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.MappingProfiles;
using BinaryTask3.BLL.Services;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BinaryTask5.BL.Tests
{
    public class TaskServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _dbContextOptions;

        public TaskServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(
                "Server=(localdb)\\mssqllocaldb;Database=ProjectsDatabaseTaskServiceTest;Trusted_Connection=True;").Options;

            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            }));

            _ = Seed();
        }

        private async Task Seed()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                await context.Database.EnsureDeletedAsync();
                await context.Database.EnsureCreatedAsync();
            }
        }

        [Fact]
        public async Task UpdateTask_WhenCall_ThenOldTaskDoesNotExistAndNewExists()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var taskService = new TaskService(new TaskRepository(context), _mapper);
                await taskService.UpdateTaskAsync(new TaskDTO() {Id = 1, Name = "TestTask"});

                Assert.Contains("TestTask", context.Tasks.Select(i => i.Name));
            }
        }

        [Fact]
        public async Task UpdateTask_WhenCallById0_ThenDbUpdateConcurrencyException()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var taskService = new TaskService(new TaskRepository(context), _mapper);

                await Assert.ThrowsAsync<DbUpdateConcurrencyException>(async () => await taskService.UpdateTaskAsync(new TaskDTO() {Id = 0, Name = "TestTask"}));
            }

        }

    }
}
