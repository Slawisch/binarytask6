﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.BLL.MappingProfiles;
using BinaryTask3.BLL.Services;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Repositories;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BinaryTask5.BL.Tests
{
    public class UserServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _dbContextOptions;

        public UserServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(
                "Server=(localdb)\\mssqllocaldb;Database=ProjectsDatabaseUserServiceTest;Trusted_Connection=True;").Options;

            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            }));

            Seed();
        }

        private async Task Seed()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                await context.Database.EnsureDeletedAsync();
                await context.Database.EnsureCreatedAsync();

                await context.SaveChangesAsync();
            }
        }

        [Fact]
        public async Task CreateUser_WhenCallTo_ThenCreationIsHappened()
        {
            var userService = A.Fake<IUserService>();
            await userService.CreateUserAsync(new UserDTO());

            A.CallTo(
                () => userService.CreateUserAsync(A<UserDTO>._)).MustHaveHappenedOnceOrMore();
        }

        [Fact]
        public async Task CreateUser_WhenAlreadyExists_ThenArgumentException()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var userService = new UserService(new UserRepository(context), _mapper);

                await Assert.ThrowsAsync<ArgumentException>(async () => await userService.CreateUserAsync(new UserDTO() {Id = 1}));
            }

        }

        [Fact]
        public async Task CreateUser_WhenTeamId1_ThenTeamWithId1HasUser()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var userService = new UserService(new UserRepository(context), _mapper);

                await userService.CreateUserAsync(new UserDTO() {TeamId = 1, FirstName = "TestUserNameForTeam1"});

                Assert.Contains("TestUserNameForTeam1",
                    context.Users.Where(i => i.TeamId == 1).Select(u => u.FirstName));
            }

        }

    }
}
