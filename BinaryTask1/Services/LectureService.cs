﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BinaryTask1.Models;
using Newtonsoft.Json;
using Task = BinaryTask1.Models.Task;
using Timer = System.Timers.Timer;

namespace BinaryTask1.Services
{
    class LectureService
    {
        private readonly HttpService _httpService;
        public Action<string> NotifyAction;

        public LectureService(HttpService service)
        {
            _httpService = service;
        }

        public async Task<Dictionary<Project, int>> GetProjectTaskCountByUser(int id)
        {
            string jsonData = await _httpService.GetProjectTaskCountByUser(id);
            string jsonPretty = JsonConvert.DeserializeObject<string>(jsonData);
            var result = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<Project, int>>>(jsonPretty);
            return result.ToDictionary(i => i.Key, i => i.Value);
        }

        public async Task<IEnumerable<BinaryTask1.Models.Task>> GetTasksByUserLess45(int id)
        {
            string jsonData = await _httpService.GetTasksByUserLess45(id);
            var result = JsonConvert.DeserializeObject<IEnumerable<Models.Task>>(jsonData);
            return result ?? new List<Models.Task>();
        }

        public async Task<IEnumerable<(int, string)>> GetTasksByUserDone(int id)
        {
            string jsonData = await _httpService.GetTasksByUserDone(id);
            var result = JsonConvert.DeserializeObject<IEnumerable<(int, string)>>(jsonData);
            return result ?? new List<(int, string)>();
        }

        public async Task<Dictionary<int, IEnumerable<User>>> GetTeamsWithUsersOlder10()
        {
            string jsonData = await _httpService.GetTeamsWithUsersOlder10();
            var result = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<int, IEnumerable<BinaryTask1.Models.User>>>>(jsonData);
            return result.ToDictionary(i => i.Key, i => i.Value);
        }

        public async Task<Dictionary<User, IEnumerable<BinaryTask1.Models.Task>>> GetUsersWithTasks()
        {
            string jsonData = await _httpService.GetUsersWithTasks();
            string jsonPretty = JsonConvert.DeserializeObject<string>(jsonData);
            var result = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<User, IEnumerable<BinaryTask1.Models.Task>>>>(jsonPretty);
            return result.ToDictionary(i => i.Key, i => i.Value);
        }

        public async Task<UserStruct> GetUserStruct(int id)
        {
            string jsonData = await _httpService.GetUserStruct(id);
            var result = JsonConvert.DeserializeObject<UserStruct>(jsonData);
            return result;
        }

        public async Task<IEnumerable<ProjectStruct>> GetProjectStruct()
        {
            string jsonData = await _httpService.GetProjectStruct();
            var result = JsonConvert.DeserializeObject<IEnumerable<ProjectStruct>>(jsonData);
            return result;
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            Random rnd = new Random();
            int id = 0;
            var tcs = new TaskCompletionSource<int>();
            Timer timer = new Timer(delay);
            timer.AutoReset = true;

            timer.Elapsed += async (sender, args) =>
            {
                try
                {
                    string jsonData = await _httpService.GetTasks();
                    var tasks = JsonConvert.DeserializeObject<IEnumerable<Task>>(jsonData);

                    id = tasks.Select(t => t.Id).ElementAt(rnd.Next(0, tasks.Count()));

                    await _httpService.MarkTaskAsDone(JsonConvert.SerializeObject(id));

                    NotifyAction.Invoke($"Marked task with id: {id}");

                    tcs.TrySetResult(id);
                }
                catch (ArgumentException e)
                {
                    NotifyAction.Invoke($"Tried to mark task with id: {id}");
                    tcs.TrySetException(e);
                }
            };

            timer.Start();

            return await tcs.Task;
        }

        public async System.Threading.Tasks.Task MarkAllTasksAsUndone()
        {
            await _httpService.MarkAllTasksAsUndone();
        }

        public async Task<IEnumerable<Task>> GetTasks()
        {
            string jsonData = await _httpService.GetTasks();
            var result = JsonConvert.DeserializeObject<IEnumerable<Task>>(jsonData);
            return result;
        }
    }
}