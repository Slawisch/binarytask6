﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTask1.Services
{
    class HttpService
    {
        private HttpClient _httpClient;
        private Uri _baseUri = new Uri("https://localhost:44322/");

        public HttpService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            
        }

        public async Task<string> GetProjectTaskCountByUser(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/custom/project_task_by_user/{id}");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }

        }

        public async Task<string> GetTasksByUserLess45(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/Custom/tasks_by_user/{id}");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }

        public async Task<string> GetTasksByUserDone(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"tasks_by_user_done/{id}");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }

        public async Task<string> GetTeamsWithUsersOlder10()
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/Custom/teams_users");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }

        public async Task<string> GetUsersWithTasks()
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/Custom/users_task");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }

        public async Task<string> GetUserStruct(int id)
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/Custom/user_structs/{id}");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }

        public async Task<string> GetProjectStruct()
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/Custom/projects_structs");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }

        public async Task<string> MarkTaskAsDone(string id)
        {
            try
            {
                var response = await _httpClient.PutAsync(_baseUri + $"api/Tasks/mark_done", new StringContent(id, Encoding.UTF8, "application/json"));
                if(response.IsSuccessStatusCode)
                    return await response.Content.ReadAsStringAsync();
                throw new ArgumentException(response.ReasonPhrase);
            }
            catch
            {
                throw;
            }
        }

        public async Task MarkAllTasksAsUndone()
        {
            try
            {
                var response = await _httpClient.PutAsync(_baseUri + $"api/Tasks/mark_all_undone", new StringContent("Empty content"));
            }
            catch
            {
                throw;
            }
        }

        public async Task<string> GetTasks()
        {
            try
            {
                var response = await _httpClient.GetAsync(_baseUri + $"api/Tasks");
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
        }


    }
}
