﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryTask3.DAL.Interfaces
{
    public interface IRepository<T> where T: class
    {
        Task<IEnumerable<T>> ReadAsync();
        Task<T> ReadAsync(int id);
        Task UpdateAsync(T project);
        Task CreateAsync(T task);
        Task DeleteAsync(int id);

        Task SaveChangesAsync();
    }
}
