﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BinaryTask3.DAL.Repositories
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        public readonly ProjectsContext _db;

        public TaskRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public async Task<IEnumerable<TaskEntity>> ReadAsync()
        {
            return await _db.Tasks.ToListAsync();
        }

        public async Task<TaskEntity> ReadAsync(int id)
        {
            var task = await _db.Tasks.FindAsync(id);

            if (task != null )
                return task;

            throw new ArgumentException($"Task with id({id}) not found");
        }

        public async Task UpdateAsync(TaskEntity task)
        {
            await Task.Run(() => _db.Tasks.Update(task));
        }

        public async Task CreateAsync(TaskEntity task)
        {
            if (task.Id == null)
            {
                await _db.Tasks.AddAsync(task);
            }
            else
                throw new ArgumentException($"Id is not equal to null, do not specify id.");
        }

        public async Task DeleteAsync(int id)
        {
            var taskForDelete = await _db.Tasks.FindAsync(id);
            if (taskForDelete != null)
                _db.Tasks.Remove(taskForDelete);
            else
                throw new ArgumentException($"Task with id({id}) not found");
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
