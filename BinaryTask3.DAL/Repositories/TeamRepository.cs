﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BinaryTask3.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        public readonly ProjectsContext _db;

        public TeamRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public async Task<IEnumerable<Team>> ReadAsync()
        {
            return await _db.Teams.ToListAsync();
        }

        public async Task<Team> ReadAsync(int id)
        {
            var team = await _db.Teams.FindAsync(id);
            if (team != null)
                return team;
            throw new ArgumentException($"Team with id({id}) not found");
        }

        public async Task UpdateAsync(Team team)
        {
            await Task.Run(() => _db.Teams.Update(team));
        }

        public async Task CreateAsync(Team team)
        {
            if (team.Id == null)
                await _db.Teams.AddAsync(team);
            else
                throw new ArgumentException($"Id is not equal to null, do not specify id.");
        }

        public async Task DeleteAsync(int id)
        {
            var teamForDelete = await _db.Teams.FindAsync(id);
            if (teamForDelete != null)
                _db.Teams.Remove(teamForDelete);
            else
                throw new ArgumentException($"Team with id({id}) not found");
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
