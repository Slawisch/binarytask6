﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BinaryTask3.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ProjectsContext _db;

        public UserRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public async Task<IEnumerable<User>> ReadAsync()
        {
            return await _db.Users.ToListAsync();
        }

        public async Task<User> ReadAsync(int id)
        {
            var user = await _db.Users.FindAsync(id);
            if (user != null)
                return user;
            throw new ArgumentException($"User with id({id}) not found");
        }

        public async Task UpdateAsync(User user)
        {
            await Task.Run(() => _db.Users.Update(user));
        }

        public async Task CreateAsync(User user)
        {
            if (user.Id == null)
            {
                await _db.Users.AddAsync(user);
            }
            else
                throw new ArgumentException($"Id is not equal to null, do not specify id.");
        }

        public async Task DeleteAsync(int id)
        {
            var userForDelete = await _db.Users.FindAsync(id);
            if (userForDelete != null)
                _db.Users.Remove(userForDelete);
            else
                throw new ArgumentException($"User with id({id}) not found");
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
