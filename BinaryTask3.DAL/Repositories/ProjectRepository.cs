﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BinaryTask3.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ProjectsContext _db;

        public ProjectRepository(ProjectsContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<Project>> ReadAsync()
        {
            return await _db.Projects.ToListAsync();
        }

        public async Task<Project> ReadAsync(int id)
        {
            var project = await _db.Projects.FindAsync(id);

            if(project != null)
                return project;

            throw new ArgumentException($"Project with id({id}) not found");
        }

        public async Task UpdateAsync(Project project)
        {
            await Task.Run(() => _db.Projects.Update(project));
        }

        public async Task CreateAsync(Project project)
        {
            if(project.Id == null)
                await _db.Projects.AddAsync(project);
            else
                throw new ArgumentException($"Id is not equal to null, do not specify id.");
        }

        public async Task DeleteAsync(int id)
        {
            var projectForDelete = await _db.Projects.FindAsync(id);
            if (projectForDelete != null)
                _db.Projects.Remove(projectForDelete);
            else
                throw new ArgumentException($"Project with id({id}) not found");
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
