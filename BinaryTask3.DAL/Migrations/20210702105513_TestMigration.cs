﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BinaryTask3.DAL.Migrations
{
    public partial class TestMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAtDate", "DeadlineDate" },
                values: new object[] { new DateTime(2021, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(6463), new DateTime(2021, 7, 7, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(6704) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAtDate",
                value: new DateTime(2021, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(4294));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "BirthDay",
                value: new DateTime(1999, 7, 2, 13, 55, 12, 893, DateTimeKind.Local).AddTicks(2778));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(3374), new DateTime(2021, 6, 17, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(3403) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAtDate", "DeadlineDate" },
                values: new object[] { new DateTime(2021, 6, 30, 18, 27, 24, 894, DateTimeKind.Local).AddTicks(1531), new DateTime(2021, 7, 5, 18, 27, 24, 894, DateTimeKind.Local).AddTicks(1776) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAtDate",
                value: new DateTime(2021, 6, 30, 18, 27, 24, 894, DateTimeKind.Local).AddTicks(152));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 30, 18, 27, 24, 893, DateTimeKind.Local).AddTicks(9335));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "BirthDay",
                value: new DateTime(1999, 6, 30, 18, 27, 24, 891, DateTimeKind.Local).AddTicks(7381));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 6, 30, 18, 27, 24, 893, DateTimeKind.Local).AddTicks(8405), new DateTime(2021, 6, 15, 18, 27, 24, 893, DateTimeKind.Local).AddTicks(8436) });
        }
    }
}
