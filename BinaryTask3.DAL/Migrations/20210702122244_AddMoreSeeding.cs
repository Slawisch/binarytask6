﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BinaryTask3.DAL.Migrations
{
    public partial class AddMoreSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAtDate", "DeadlineDate", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 7, 2, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(9693), new DateTime(2021, 7, 7, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(9942), "DescriptionForProject1", "Project1", 1 },
                    { 2, 1, new DateTime(2021, 7, 2, 15, 22, 44, 294, DateTimeKind.Local).AddTicks(807), new DateTime(2021, 7, 7, 15, 22, 44, 294, DateTimeKind.Local).AddTicks(816), "DescriptionForProject2", "Project2", 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAtDate", "Description", "FinishedAtDate", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 2, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(7262), "Task1Description", new DateTime(2021, 7, 2, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(8539), "Task1Name", 2, 1, 0 },
                    { 2, new DateTime(2021, 7, 2, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(9135), "Task2Description", null, "Task2Name", 1, 2, 0 }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 7, 2, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(6391), "Team1" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(1999, 7, 2, 15, 22, 44, 291, DateTimeKind.Local).AddTicks(3980), "EmailText", "UserFirstName1", "UserLastName1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, new DateTime(1991, 7, 2, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(5734), "Email@gmail.com", "UserFirstName2", "UserLastName2", new DateTime(2021, 6, 17, 15, 22, 44, 293, DateTimeKind.Local).AddTicks(5764), 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAtDate", "DeadlineDate", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2021, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(9102), new DateTime(2021, 7, 7, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(9344), null, null, 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAtDate", "Description", "FinishedAtDate", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2021, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(7740), null, null, null, 2, 1, 0 });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(6923), "Team1" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(1999, 7, 2, 14, 15, 33, 409, DateTimeKind.Local).AddTicks(4507), "EmailText", "UserFirstName1", "UserLastName1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 2, new DateTime(1991, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(5992), "Email@gmail.com", "UserFirstName2", "UserLastName2", new DateTime(2021, 6, 17, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(6022), 1 }
                });
        }
    }
}
