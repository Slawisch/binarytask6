﻿using System;
using System.ComponentModel.DataAnnotations;

#nullable enable

namespace BinaryTask3.DAL.Entities
{
    public class Team : BaseEntity
    {
        [StringLength(20)]
        public string? Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
