﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using Microsoft.AspNetCore.Http;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Tasks")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            return await _taskService.GetTasksAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTask(int id)
        {
            try
            {
                return new JsonResult(await _taskService.GetTaskAsync(id));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPost]
        public async Task<IActionResult> AddTeam([FromBody] TaskDTO task)
        {
            try
            {
                await _taskService.CreateTaskAsync(task);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status409Conflict, e.Message);
            }

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            try
            {
                await _taskService.DeleteTaskAsync(id);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPut]
        public async Task<IActionResult> UpdateTask([FromBody] TaskDTO task)
        {
            try
            {
                await _taskService.UpdateTaskAsync(task);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPut("mark_done")]
        public async Task<IActionResult> MarkTaskAsDone([FromBody]int id)
        {
            try
            {
                await _taskService.MarkTaskAsDoneAsync(id);
                return new OkResult();
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(StatusCodes.Status409Conflict, e.Message);
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPut("mark_all_undone")]
        public async Task<IActionResult> MarkAllAsUndone()
        {
            try
            {
                await _taskService.MarkAllTasksAsUndoneAsync();
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }
    }
}
