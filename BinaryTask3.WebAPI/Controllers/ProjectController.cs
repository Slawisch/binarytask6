﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using Microsoft.AspNetCore.Http;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Projects")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            return await _projectService.GetProjectsAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProject(int id)
        {
            try
            {
                return new JsonResult(await _projectService.GetProjectAsync(id));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> AddProject([FromBody] ProjectDTO project)
        {
            try
            {
                await _projectService.CreateProjectAsync(project);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status409Conflict, e.Message);
            }
            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            try
            {
                await _projectService.DeleteProjectAsync(id);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProject([FromBody] ProjectDTO project)
        {
            try
            {
                await _projectService.UpdateProjectAsync(project);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            
        }

    }
}
